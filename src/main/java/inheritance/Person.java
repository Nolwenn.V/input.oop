package inheritance;

public class Person implements IHouseItem {
    public String greeting(String name) {

        return "Hello, " + name + ", I am human";
    }

    public String tellName() {
        return "I am Nolwenn";
    }

    @Override
    public void use() {
        greeting("everybody");

    }

    @Override
    public boolean canMove() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public String draw() {

        return "🧍";
    }

}
