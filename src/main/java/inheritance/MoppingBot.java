package inheritance;

public class MoppingBot extends CleaningBot {
    boolean dryCleaning = true;
    int waterLevel = 100;

    public void toggleDryCleaning() {
        dryCleaning = !dryCleaning;

    }

    @Override
    public void clean() {
        if (!dryCleaning && waterLevel > 5) {
            waterLevel -= 5;
            discharge(15);
        } else {
            super.clean();
        }
    }

}
