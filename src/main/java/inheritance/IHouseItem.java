package inheritance;

public interface IHouseItem {
    public void use();

    public boolean canMove();

    public String draw();

}
