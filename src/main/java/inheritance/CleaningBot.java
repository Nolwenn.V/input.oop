package inheritance;

public class CleaningBot extends Robot {
    int dustBag = 0;

    public void clean() {
        if (isOn() == true) {
            discharge(5);
            if (dustBag < 100) {
                dustBag += 5;

            }
            System.out.println("la li la lou I'm cleaning the house");
        }

    }

    @Override
    public void use() {
        clean();

    }

    @Override
    public boolean canMove() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public String draw() {
        return "🧹";
    }

}
