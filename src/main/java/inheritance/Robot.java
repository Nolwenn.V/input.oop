package inheritance;

import java.util.UUID;

public class Robot implements IHouseItem {
    private int battery = 100;

    public Robot() {
    }

    public Robot(int battery) {
        this.battery = battery;
    }

    String serialNumber = UUID.randomUUID().toString();

    public int charge(int value) {
        battery += value;
        if (battery > 100) {
            battery = 100;

        }
        return battery;

    }

    protected int discharge(int value) {
        battery -= value;
        if (battery < 0) {
            battery = 0;

        }
        return battery;

    }

    public boolean isOn() {
        if (battery > 0) {
            return true;

        }
        return false;
    }

    @Override
    public void use() {
        discharge(5);

    }

    @Override
    public boolean canMove() {
        if (battery < 0) {
            return false;

        }
        return true;
    }

    @Override
    public String draw() {
        return "🤖";
    }

}
