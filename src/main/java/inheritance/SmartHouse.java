package inheritance;

import java.util.ArrayList;
import java.util.List;

public class SmartHouse {
    private List<IHouseItem> houseItems = new ArrayList<>();
    private IHouseItem[][] grid = new IHouseItem[10][10];
    private IHouseItem selected = null;
    private Integer selectedX = null;
    private Integer selectedY = null;

    public void placeItem(IHouseItem item, int x, int y) {
        if (!houseItems.contains(item)) {
            houseItems.add(item);
            if (grid[x][y] == null) {
                grid[x][y] = item;
            }

        }
        System.out.println(grid[x][y] = item);

    }

    public void displayHouse() {
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid.length; j++) {
                IHouseItem item = grid[i][j];
                System.out.print(" ");
                if (item == null) {
                    System.out.print(".");
                } else {
                    if (item == selected) {
                        System.out.print("[" + item.draw() + "]");

                    } else {
                        System.out.print(item.draw());
                    }
                }
                System.out.print(" ");
            }
            System.out.println();

        }

    }

    public void select(int x, int y) {
        if (grid[x][y] != null) {
            selected = grid[x][y];
            selectedX = x;
            selectedY = y;

        }

    }

    public void moveSelected(int x, int y) {
        if (selected != null && selected.canMove() && grid[x][y] == null) {
            grid[x][y] = selected;
            grid[selectedX][selectedY] = null;
            selectedX = x;
            selectedY = y;

        }

    }

    public void useSelected() {
        if (selected != null) {
            selected.use();
        }

    }
}
