package inheritance;

import java.util.Scanner;

public class HouseScanner {
    SmartHouse house;
    Scanner scan;
    String imput;
    boolean isStart = true;

    public void start() {
        while (true) {
            house.displayHouse();
            System.out.println("""
                    What do you want to do ?
                    1.Select an Item
                    2.Add an Item
                    3.Exit
                    """);
            imput = scan.nextLine();
            switch (imput) {
                case "1":
                    itemSelection();

                    break;
                case "2":
                    System.out.println("""
                            select an item :
                            1.Person
                            2.Robot
                            3.CleaningBot
                            """);
                    imput = scan.nextLine();
                    System.out.println("Enter 2 positions for the new item");
                    if (imput.equals("1")) {
                        house.placeItem(new Person(), scan.nextInt(), scan.nextInt());

                    } else if (imput.equals("2")) {
                        house.placeItem(new Robot(), scan.nextInt(), scan.nextInt());

                    } else if (imput.equals("3")) {
                        house.placeItem(new CleaningBot(), scan.nextInt(), scan.nextInt());

                    }

                    break;
                case "3":
                    isStart = false;

                    break;

                default:
                    break;
            }

        }
    }

    public HouseScanner(SmartHouse house) {
        this.house = house;
        this.scan = new Scanner(System.in);
    }

    public void itemSelection() {
        System.out.println("enter 2 positions for select an item");
        System.out.println("x");
        int x = scan.nextInt();
        System.out.println("y");
        int y = scan.nextInt();
        house.select(x, y);
        house.displayHouse();
        selectedMenu();

    }

    public void selectedMenu() {
        while (true) {

            System.out.println("""
                    What do you want to do ?
                    1.Move
                    2.Use
                    3.Back
                    """);
            int imput = scan.nextInt();
            switch (imput) {
                case 1:
                    System.out.println("Enter the new position");
                    house.moveSelected(scan.nextInt(), scan.nextInt());
                    house.displayHouse();

                    break;
                case 2:
                    house.useSelected();

                    break;
                case 3:
                    start();

                    break;

                default:
                    break;
            }
        }
    }

}
