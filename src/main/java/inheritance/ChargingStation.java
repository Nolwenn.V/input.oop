package inheritance;

public class ChargingStation {
    private int power = 22;

    public void connect(Robot robot) {
        robot.charge(power);

    }

}
