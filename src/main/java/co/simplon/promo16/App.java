package co.simplon.promo16;

import inheritance.HouseScanner;
import inheritance.Person;
import inheritance.Robot;
import inheritance.SmartHouse;
/*import todo.ScannerTodoList;
import todo.TodoList;*/

public class App {
    public static void main(String[] args) {

        /*
         * TodoList todo = new TodoList();
         * todo.addTask("faire les courses");
         * todo.addTask("faire a manger");
         * todo.addTask("manger");
         * todo.actionTask(0);
         * todo.actionTask(2);
         * System.out.println(todo.display());
         * 
         * ScannerTodoList sTodo = new ScannerTodoList();
         * sTodo.start();
         */
        SmartHouse house = new SmartHouse();
        HouseScanner houseScanner = new HouseScanner(house);

        house.placeItem(new Robot(), 2, 2);
        house.placeItem(new Person(), 5, 3);
        houseScanner.start();
    }
}