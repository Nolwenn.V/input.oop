package co.simplon.promo16;

import java.util.Random;
import java.util.Scanner;

public class GessNumber {

    private Boolean over = false;
    Random random = new Random();
    int nbADeviner = random.nextInt((100 + 1 - 0) + 0);

    public String miniGame(int x) {
        String plus = "it's greater than that";
        String moins = "it's lesser than that";
        String bravo = "perfect number, congrates";

        if (x < nbADeviner) {
            return plus;
        } else if (x > nbADeviner) {
            return moins;
        } else if (x == nbADeviner) {
            over = true;
            return bravo;

        } else {
            return "yo";
        }

    }

    public void run() {
        Scanner scanner = new Scanner(System.in);

        while (!over) {
            int input = scanner.nextInt();
            System.out.println(miniGame(input));

        }
        scanner.close();

    }
}
