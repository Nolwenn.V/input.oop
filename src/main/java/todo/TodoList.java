package todo;

import java.util.ArrayList;
import java.util.List;

public class TodoList {
    private List<Task> tasks = new ArrayList<>();


    public String addTask(String label) {
        Task task = new Task(label);
        tasks.add(task);
        return task.getUuid();

    }

    public void clearDone() {
        for (int i = 0; i < tasks.size(); i++) {
            if (tasks.get(i).isDone()) {
                tasks.remove(i);

            }

        }

    }

    public void actionTask(int index) {
        tasks.get(index).toggleDone();

    }

    public String display() {
        String toDisplay = "";

        for (Task task : tasks) {
            toDisplay += task.display() + "\n";

        }
        return toDisplay;

    }

}
