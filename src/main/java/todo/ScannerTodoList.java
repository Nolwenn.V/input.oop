package todo;


import java.util.Scanner;

public class ScannerTodoList {
    private TodoList todoList;
    private boolean over;

    public ScannerTodoList() {
        this.todoList = new TodoList();
    }

    public void start() {
        Scanner scanner = new Scanner(System.in);
        while (!over) {
            System.out.println("Enter a new task");
            String input = scanner.nextLine();
            todoList.addTask(input);
            System.out.println(todoList.display());
            
        }



        scanner.close();

    }

}
