package inheritance;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RobotTest {
    @Test
    public void testCharge() {
        Robot robot = new Robot(20);
        assertEquals(30, robot.charge(10));

    }
}
